. ~/.bashrc
echo "Loaded bashrc"
echo `which python`

# export PATH="/home/pi/miniconda3/bin:$PATH"
echo `which python`

cd /home/pi/Documents/timelapse
rm output.mp4
rm -r images
mkdir images

python3 captureImages.py

#Create the video from the files in testImages
ffmpeg -framerate 20 -i images/image%d.jpg -vf scale=1024:768 -y output.mp4

echo "Posting to twitter"
python3 postToTwitter.py

#date=$(date +'%Y.%m.%d-%H.%M.%S')

echo "Compressing"
tar cfz $date.tar.gz images output.mp4 out.log


#echo "Uploading to dropbox"
#rclone copy $date.tar.gz remote:sunrise_pictures
#rclone copy output.mp4 remote:sunrise_pictures

rm $date.tar.gz

# for i in $(seq 1 400); do
#     echo $i
#     enfuse -o exposure/enfuse/image${i}.jpg exposure/images_25/images_${i}.jpg \
#                         exposure/images_50/images_${i}.jpg \
#                         exposure/images_100/images_${i}.jpg \
# 						exposure/images_500/images_${i}.jpg \
# 						exposure/images_2500/images_${i}.jpg \
# 						exposure/images_10000/images_${i}.jpg \
# 						exposure/images_25000/images_${i}.jpg
# done

# rm exposure/enfuse-alt/*

# python convertEnfuse.py
# ffmpeg -framerate 20 -i exposure/enfuse-alt/image%d.jpg -vf scale=1024:768 -y output-enfuse.mp4
# python postToTwitter-enfuse.py
