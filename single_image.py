import twitter
import datetime
import time
from picamera import PiCamera
from fractions import Fraction
import os

## Take timelapse pictures for 2 hours and save them.
camera = PiCamera()
# camera.resolution = (2592, 1944)
# camera.rotation = 270

# camera.meter_mode = 'matrix'
# camera.awb_mode = 'horizon'
# camera.iso = 100
# camera.shutter_speed = 0
# camera.exposure_compensation = -25

print("Starting to take pictures")
camera.capture('tmp.jpg')
