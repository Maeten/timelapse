import twitter
import datetime
import time
from picamera import PiCamera
from fractions import Fraction
import os

class time_class(object) :
    def __init__(self, minutes = None, hours = None, text = None):
        if text is not None:
            minutes = int(text[-2:])
            hours = int(text[:-2])
        if hours is None:
            self.minutes = minutes
        else:
            self.minutes = 60*int(hours) + int(minutes)

    def subtract(self, minutes = None, hours = None):
        diff = 0
        if minutes is not None:
            diff += minutes
        if hours is not None:
            diff += 60*hours
        self.minutes -= diff

    def __str__(self):
        print(self.minutes)
        hours = str(self.minutes//60)
        minutes = str(self.minutes%60)
        if len(minutes) == 1:
            minutes = "0" + minutes
        return hours + minutes

def getCurrentTime():
    date = datetime.datetime.utcnow() - datetime.timedelta(hours = 5)
    hour = date.hour
    minute = date.minute
    return time_class(hours = hour, minutes = minute)

def getSunriseTime():
    date = datetime.datetime.utcnow()
    month = date.strftime("%B").lstrip("0").replace(" 0", " ")
    day = date.strftime("%d").lstrip("0").replace(" 0", " ")
    text = sunriseDictionary[month][day]
    base = time_class(text = text)
    base.subtract(minutes = 60)
    return base

def waitUntil(targetTime):
    #Simplest way.
    cTime = time.time()
    if cTime< targetTime:
        time.sleep(targetTime -cTime)


##Load start time.
sunriseDictionary = dict()
with open("sunrise_times.txt") as f:
    for line in f:
        parts = line.split()
        if parts[0] not in sunriseDictionary:
            sunriseDictionary[parts[0]] = dict()
        sunriseDictionary[parts[0]][parts[1]] = parts[3]

print(sunriseDictionary)
### Send message to say you are awake.
currentTime = getCurrentTime()
sunriseTime = getSunriseTime() #May need to back this up by half an hour.
message = "Good morning, it is currently {}. Sunrise is at {}. Everything looks good.".format(currentTime, sunriseTime)

print(message)


# ## Wait till the start time
while currentTime.minutes < sunriseTime.minutes:
    time.sleep(60)
    currentTime = getCurrentTime()
    print(currentTime)

## Take timelapse pictures for 2 hours and save them.
camera = PiCamera()
camera.resolution = (2592, 1944)
camera.rotation = 270

camera.meter_mode = 'matrix'
camera.awb_mode = 'horizon'
camera.iso = 100
camera.shutter_speed = 0
camera.exposure_compensation = 0

print("Starting to take pictures")
nPictures = 600
duration = 1.5*60*60


# nPictures = 30
# duration = 60

waitTime = duration/nPictures

cTime = time.time()
times = [cTime + i*waitTime for i in range(nPictures)]

for i in range(nPictures) :

    waitUntil(times[i])

    camera.capture('images/image{}.jpg'.format(i))
    #(Fraction(117, 64), Fraction(367, 256))
#
#    for exp in ['average','spot','backlit','matrix']:
#        suffix = str(exp)
#        if i == 0:
#            try:
#                os.mkdir("exposure/images_{}".format(suffix))
#            except:
#                pass
#        camera.meter_mode = exp
#        time.sleep(1)
#        # print(i, exp, camera.shutter_speed, camera.exposure_speed, camera.digital_gain,  camera.analog_gain, camera.awb_gains)
#
#        camera.capture('exposure/images_{}/images_{}.jpg'.format(suffix, i))
#

