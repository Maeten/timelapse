import twitter
import datetime
import time
from picamera import PiCamera

def getSunriseTime():
	date = datetime.datetime.now()
	month = date.strftime("%B")
	day = date.strftime("%d")
	return pullBack(sunriseDictionary[month][day])

def getCurrentTimeInInt():
	date = datetime.datetime.now()
	return convertDate(date)
	
def convertDate(date):
	hour = str(date.hour)
	minute = str(date.minute)
	if len(minute) == 1:
		minute = "0" + minute
	time = int(hour + minute)
	return time

def pullBack(val) :
	val = str(val)
	date = datetime.datetime.now()
	if len(val) == 3:
		hour = int(val[0])
		minute = int(val[1:2])
	if len(val) == 4:
		hour = int(val[0:1])
		minute = int(val[2:3])
	date.hour = hour
	date.minute = minute
	date = date - datetime.timedelta(minutes=30)
	return convertDate(val)

sunriseDictionary = dict()
with open("sunrise_times.txt") as f:
	for line in f:
		parts = line.split()
		if parts[0] not in sunriseDictionary:
			sunriseDictionary[parts[0]] = dict()
		sunriseDictionary[parts[0]][parts[1]] = int(parts[2])
	

currentTime = getCurrentTimeInInt()
sunriseTime = getSunriseTime() #May need to back this up by half an hour.
print(currentTime)
print(sunriseTime)
