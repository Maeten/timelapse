
for i in {1..400}; do
    echo i
    enfuse -o exposure/enfuse/image${i}.jpg exposure/images_{0,-6,-24}/image${i}.jpg
done

python testEnfuse.py
ffmpeg -framerate 20 -i exposure/enfuse-alt/image%d.jpg -vf scale=1024:768 -y output-enfuse0-24.mp4

i=1
enfuse -o exposure/enfuse/image${i}.jpg exposure/images_{100,500,2500,10000,25000,50000,100000}/images_${i}.jpg

for type in matrix backlit spot average; do
    ffmpeg -framerate 20 -i exposure/images_${type}/images_%d.jpg -vf scale=1024:768 -y output_${type}.mp4
done

